package uppenbar.abelian

trait Element {

  def *[A <: Element, B <: Element](factor: A)(implicit operation: Operation[this.type, A, B]): B =
    operation(this, factor)

  def /[A <: Element, B <: Element](denominator: A)(implicit operation: Operation[this.type, denominator.unary_~.type, B]): B =
    operation(this, ~denominator)

  def terms: Map[Basic, Int]

  val unary_~ : Element
}
