package uppenbar.abelian

trait ~[A <: Basic] extends Simple {

  def terms = Map(~this -> -1)

  override def toString = s"${unary_~}-1"

  val unary_~ : A
}
