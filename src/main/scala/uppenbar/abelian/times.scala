package uppenbar.abelian

class *[A <: NonIdentity, B <: Simple] private (val a: A, val b: B)(implicit canonically: A Before B)
    extends NonIdentity {

  override def equals(that: Any): Boolean = that match {
    case thatA * thatB => a == thatA && b == thatB
    case _ => false
  }

  override def hashCode = terms.hashCode

  def terms = (a.terms /: b.terms) { case (terms, (term, power)) =>
    terms + (term -> (terms.getOrElse(term, 0) + power))
  }

  override def toString = s"$a.$b"

  lazy val unary_~ = ??? // a.inverse * b.inverse
}

object * {
  def apply[A <: NonIdentity, B <: Simple](a: A, b: B)(implicit canonically: A Before B)=
    new *(a, b)

  def unapply[A <: NonIdentity, B <: Simple](element: *[A, B]) =
    Some((element.a, element.b))
}
