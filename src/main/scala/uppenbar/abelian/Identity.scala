package uppenbar.abelian

object Identity extends Element {

  val terms = Map[Basic, Int]()

  override def toString = "1"

  val unary_~ = this
}
