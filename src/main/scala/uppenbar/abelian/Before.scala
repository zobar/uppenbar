package uppenbar.abelian

class Before[-A <: NonIdentity, -B <: Simple] private {
}

object Before {

  def apply[A <: NonIdentity, B <: Simple] = new Before[A, B]

  implicit def `a=a`[A <: Basic, B <: Basic](implicit evidence: A =:= B) = Before[A, B]

  implicit def `a>b-1`[A <: Basic, B <: Basic](implicit canonically: A Before B) = Before[A, ~[B]]

  implicit def `a*b>c`[A <: NonIdentity, B <: Simple, C <: Simple]
    (implicit canonicalA: A Before C, canonicalB: B Before C) = Before[*[A, B], C]

  implicit def `b>a-1`[A <: Basic, B <: Basic](implicit canonically: A Before B) = Before[B, ~[A]]

  implicit def `b-1>a-1`[A <: Basic, B <: Basic](implicit canonically: A Before B) = Before[~[B], ~[A]]
}
