package uppenbar.abelian

trait Basic extends Simple {
  basic =>

  protected[this] def before[A <: Basic] = Before[this.type, A]

  val terms = Map(this -> 1)

  object unary_~ extends ~[this.type] {

    val unary_~ : basic.type = basic
  }
}
