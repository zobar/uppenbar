package uppenbar.abelian

trait Operation[-A, -B, +C] extends ((A, B) => C)

object Operation {

  def apply[A, B, C](operation: (A, B) => C): Operation[A, B, C] =
    new Operation[A, B, C] { def apply(a: A, b: B): C = operation(a, b) }

  implicit def `a*a*a-1`[A <: Basic] =
    Operation((a: *[A, A], b: ~[A]) => a.a)

  implicit def `a*b`[A <: NonIdentity, B <: Simple](implicit canonically: A Before B) =
    Operation((a: A, b: B) => *(a, b))

  implicit def `a*b*a-1`[A <: Basic, B <: Simple] =
    Operation((a: *[A, B], b: ~[A]) => a.b)

  implicit def `a*b*b-1`[A <: NonIdentity, B <: Basic] =
    Operation((a: *[A, B], b: ~[B]) => a.a)

  implicit def `a*1`[A <: Element] =
    Operation((a: A, b: Identity.type) => a)

  implicit def `a*a-1`[A <: Basic] =
    Operation((a: A, b: ~[A]) => Identity)

  implicit def `1*a-1`[A <: Basic] =
    Operation((a: Identity.type, b: ~[A]) => b)

  implicit def commutativity[A, B, C](implicit operation: Operation[A, B, C]) =
    Operation((a: B, b: A) => operation(b, a))
}
