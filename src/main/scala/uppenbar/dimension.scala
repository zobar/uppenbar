package uppenbar
package dimension

import abelian.{Basic => Dimension}

object ElectricCharge extends Dimension {

  implicit def beforeLength = before[Length.type]

  implicit def beforeLuminousIntensity = before[LuminousIntensity.type]

  implicit def beforeMass = before[Mass.type]

  implicit def beforePlaneAngle = before[PlaneAngle.type]

  implicit def beforeTemperature = before[Temperature.type]

  implicit def beforeTime = before[Time.type]

  override def toString = "[charge]"
}

object Length extends Dimension {

  implicit def beforeLuminousIntensity = before[LuminousIntensity.type]

  implicit def beforeMass = before[Mass.type]

  implicit def beforePlaneAngle = before[PlaneAngle.type]

  implicit def beforeTemperature = before[Temperature.type]

  implicit def beforeTime = before[Time.type]

  override def toString = "[length]"
}

object LuminousIntensity extends Dimension {

  implicit def beforeMass = before[Mass.type]

  implicit def beforePlaneAngle = before[PlaneAngle.type]

  implicit def beforeTemperature = before[Temperature.type]

  implicit def beforeTime = before[Time.type]

  override def toString = "[luminous intensity]"
}

object Mass extends Dimension {

  implicit def beforePlaneAngle = before[PlaneAngle.type]

  implicit def beforeTemperature = before[Temperature.type]

  implicit def beforeTime = before[Time.type]

  override def toString = "[mass]"
}

object PlaneAngle extends Dimension {

  implicit def beforeTemperature = before[Temperature.type]

  implicit def beforeTime = before[Time.type]

  override def toString = "[angle]"
}

object Temperature extends Dimension {

  implicit def beforeTime = before[Time.type]

  override def toString = "[temperature]"
}

object Time extends Dimension {

  override def toString = "[time]"
}
