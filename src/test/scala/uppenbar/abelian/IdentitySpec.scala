package uppenbar.spec
package abelian

import uppenbar.abelian.Identity

class IdentitySpec extends Spec {

  "terms" should "be empty" in {
    Identity.terms shouldBe empty
  }

  "1 * A" should "== A" in {
    (Identity * A) should equal(A)
  }

  "1 * 1" should "== 1" in {
    (Identity * Identity) should equal(Identity)
  }

  "1 * ~A" should "== ~A" in {
    (Identity * ~A) should equal(~A)
  }
}
