package uppenbar.spec
package abelian

import uppenbar.abelian.Identity

class `*Spec` extends Spec {
  "A * A * A" should "== (A * A * A)" in {
    (A * A * A) should equal(A * A * A)
  }

  "A * A * B" should "== (A * A * B)" in {
    (A * A * B) should equal(A * A * B)
  }

  "A * A * C" should "== (A * A * C)" in {
    (A * A * C) should equal(A * A * C)
  }

  "A * A * 1" should "== (A * A)" in {
    (A * A * Identity) should equal(A * A)
  }

  "A * A * ~C" should "== (A * A * ~C)" in {
    (A * A * ~C) should equal(A * A * ~C)
  }

  "A * A * ~B" should "== (A * A * ~B)" in {
    (A * A * ~B) should equal(A * A * ~B)
  }

  "A * A * ~A" should "== (A)" in {
    (A * A * ~A) should equal(A)
  }

  "A * B * B" should "== (A * B * B)" in {
    (A * B * B) should equal(A * B * B)
  }

  "A * B * C" should "== (A * B * C)" in {
    (A * B * C) should equal(A * B * C)
  }

  "A * B * 1" should "== (A * B)" in {
    (A * B * Identity) should equal(A * B)
  }

  "A * B * ~C" should "== (A * B * ~C)" in {
    (A * B * ~C) should equal(A * B * ~C)
  }

  "A * B * ~B" should "== A" in {
    (A * B * ~B) should equal(A)
  }

  "A * B * ~A" should "== B" in {
    (A * B * ~A) should equal(B)
  }

  // "A * C * B" should "== (A * B * C)" in {
  //   (A * C * B) should equal(A * B * C)
  // }

  "A * C * C" should "== (A * C * C)" in {
    (A * C * C) should equal(A * C * C)
  }

  "A * C * 1" should "== (A * C)" in {
    (A * C * Identity) should equal(A * C)
  }

  "A * C * ~C" should "== A" in {
    (A * C * ~C) should equal(A)
  }

  "A * C * ~B" should "== (A * C * ~B)" in {
    (A * C * ~B) should equal(A * C * ~B)
  }

  "A * C * ~A" should "== C" in {
    (A * C * ~A) should equal(C)
  }

  "A * ~C * ~C" should "== (A * ~C * ~C)" in {
    (A * ~C * ~C) should equal(A * ~C * ~C)
  }

  "A * ~C * ~B" should "== (A * ~C * ~B)" in {
    (A * ~C * ~B) should equal(A * ~C * ~B)
  }

  "A * ~C * ~A" should "== ~C" in {
    (A * ~C * ~A) should equal(~C)
  }

  "A * ~B * ~B" should "== (A * ~B * ~B)" in {
    (A * ~B * ~B) should equal(A * ~B * ~B)
  }

  "A * ~B * ~A" should "== ~B" in {
    (A * ~B * ~A) should equal(~B)
  }

  "A * ~A * ~A" should "== (A * ~A * ~A)" in {
    (A * ~A * ~A) should equal(A * ~A * ~A)
  }

  "B * B * B" should "== (B * B * B)" in {
    (B * B * B) should equal(B * B * B)
  }

  "B * B * C" should "== (B * B * C)" in {
    (B * B * C) should equal(B * B * C)
  }

  "B * B * 1" should "== (B * B)" in {
    (B * B * Identity) should equal(B * B)
  }

  "B * B * ~C" should "== (B * B * ~C)" in {
    (B * B * ~C) should equal(B * B * ~C)
  }

  "B * B * ~B" should "== B" in {
    (B * B * ~B) should equal(B)
  }

  "B * B * ~A" should "== (B * B * ~A)" in {
    (B * B * ~A) should equal(B * B * ~A)
  }

  "B * C * C" should "== (B * C * C)" in {
    (B * C * C) should equal(B * C * C)
  }

  "B * C * 1" should "== (B * C)" in {
    (B * C * Identity) should equal(B * C)
  }

  "B * C * ~C" should "== B" in {
    (B * C * ~C) should equal(B)
  }

  "B * C * ~B" should "== C" in {
    (B * C * ~B) should equal(C)
  }

  "B * C * ~A" should "== (B * C * ~A)" in {
    (B * C * ~A) should equal(B * C * ~A)
  }

  "B * ~C * ~C" should "== (B * ~C * ~C)" in {
    (B * ~C * ~C) should equal(B * ~C * ~C)
  }

  "B * ~C * ~B" should "== ~C" in {
    (B * ~C * ~B) should equal(~C)
  }

  "B * ~C * ~A" should "== (B * ~C * ~A)" in {
    (B * ~C * ~A) should equal(B * ~C * ~A)
  }

  "B * ~A * ~A" should "== (B * ~A * ~A)" in {
    (B * ~A * ~A) should equal(B * ~A * ~A)
  }

  "C * C * C" should "== (C * C * C)" in {
    (C * C * C) should equal(C * C * C)
  }

  "C * C * 1" should "== (C * C)" in {
    (C * C * Identity) should equal(C * C)
  }

  "C * C * ~C" should "== C" in {
    (C * C * ~C) should equal(C)
  }

  "C * C * ~B" should "== (C * C * ~B)" in {
    (C * C * ~B) should equal(C * C * ~B)
  }

  "C * C * ~A" should "== (C * C * ~A)" in {
    (C * C * ~A) should equal(C * C * ~A)
  }

  "C * ~B * ~B" should "== (C * ~B * ~B)" in {
    (C * ~B * ~B) should equal(C * ~B * ~B)
  }

  "C * ~B * ~A" should "== (C * ~B * ~A)" in {
    (C * ~B * ~A) should equal(C * ~B * ~A)
  }

  "~C * ~C * ~C" should "== (~C * ~C * ~C)" in {
    (~C * ~C * ~C) should equal(~C * ~C * ~C)
  }

  "~C * ~C * ~B" should "== (~C * ~C * ~B)" in {
    (~C * ~C * ~B) should equal(~C * ~C * ~B)
  }

  "~C * ~C * ~A" should "== (~C * ~C * ~A)" in {
    (~C * ~C * ~A) should equal(~C * ~C * ~A)
  }

  "~C * ~B * ~B" should "== (~C * ~B * ~B)" in {
    (~C * ~B * ~B) should equal(~C * ~B * ~B)
  }

  "~C * ~B * ~A" should "== (~C * ~B * ~A)" in {
    (~C * ~B * ~A) should equal(~C * ~B * ~A)
  }

  "~C * ~A * ~A" should "== (~C * ~A * ~A)" in {
    (~C * ~A * ~A) should equal(~C * ~A * ~A)
  }

  "~B * ~B * ~B" should "== (~B * ~B * ~B)" in {
    (~B * ~B * ~B) should equal(~B * ~B * ~B)
  }

  "~B * ~B * ~A" should "== (~B * ~B * ~A)" in {
    (~B * ~B * ~A) should equal(~B * ~B * ~A)
  }

  "~B * ~A * ~A" should "== (~B * ~A * ~A)" in {
    (~B * ~A * ~A) should equal(~B * ~A * ~A)
  }

  "~A * ~A * ~A" should "== (~A * ~A * ~A)" in {
    (~A * ~A * ~A) should equal(~A * ~A * ~A)
  }
}
