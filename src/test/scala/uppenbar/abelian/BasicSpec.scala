package uppenbar.spec
package abelian

import uppenbar.abelian.Identity

class BasicSpec extends Spec {

  "terms" should "== Map(A -> 1)" in {
    A.terms should equal(Map(A -> 1))
  }

  "A * A" should "== (A * A)" in {
    (A * A) should equal(A * A)
  }

  "A * B" should "== (A * B)" in {
    (A * B) should equal(A * B)
  }

  "A * 1" should "== A" in {
    (A * Identity) should equal(A)
  }

  "A * ~B" should "== (A * ~B)" in {
    (A * ~B) should equal(A * ~B)
  }

  "A * ~A" should "== 1" in {
    (A * ~A) should equal(Identity)
  }

  "B * A" should "== (A * B)" in {
    (B * A) should equal(A * B)
  }

  "B * B" should "== (B * B)" in {
    (B * B) should equal(B * B)
  }

  "B * 1" should "== B" in {
    (B * Identity) should equal(B)
  }

  "B * ~B" should "== 1" in {
    (B * ~B) should equal(Identity)
  }

  "B * ~A" should "== (B * ~A)" in {
    (B * ~A) should equal(B * ~A)
  }
}
