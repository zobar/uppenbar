package uppenbar.spec

import uppenbar.abelian.Basic

object A extends Basic {

  implicit def before_B = before[B.type]

  implicit def before_C = before[C.type]

  override def toString = "A"
}

object B extends Basic {

  implicit def before_C = before[C.type]

  override def toString = "B"
}

object C extends Basic {

  override def toString = "C"
}
