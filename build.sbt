libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.0" % "test"
)

name := "uppenbar"

scalacOptions ++= Seq("-feature")

scalaVersion := "2.10.3"

version := "0.0.0"
